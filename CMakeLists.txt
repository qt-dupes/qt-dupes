cmake_minimum_required(VERSION 3.10)

project(qt-dupes)

add_subdirectory("ldupes-cpp")
add_subdirectory("gui")

add_executable(app
    main.cpp)

target_link_libraries(app ldupes-cpp gui)

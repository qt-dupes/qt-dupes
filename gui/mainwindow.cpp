#include "../ldupes-cpp/ldupes.hpp"

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QCommonStyle>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QThread>

#include <cassert>

main_window::main_window(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));

    ui->treeWidget->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->treeWidget->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents);

    QCommonStyle style;
    ui->actionScan_Directory->setIcon(style.standardIcon(QCommonStyle::SP_DialogOpenButton));
    ui->actionExit->setIcon(style.standardIcon(QCommonStyle::SP_DialogCloseButton));
    ui->actionAbout->setIcon(style.standardIcon(QCommonStyle::SP_DialogHelpButton));
    ui->actionCancel_Scanning->setIcon(style.standardIcon(QCommonStyle::SP_DialogCancelButton));
    ui->actionDelete_Selected->setIcon(style.standardIcon(QCommonStyle::SP_TrashIcon));

    connect(ui->actionScan_Directory, &QAction::triggered, this, &main_window::select_directory);
    connect(ui->actionExit, &QAction::triggered, this, &QWidget::close);
    connect(ui->actionAbout, &QAction::triggered, this, &main_window::show_about_dialog);
    connect(ui->actionCancel_Scanning, &QAction::triggered, this, &main_window::cancel_scanning);
    connect(ui->actionDelete_Selected, &QAction::triggered, this, &main_window::ask_for_deletion);

    connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked, this, &main_window::open_for_editing);

    connect(this, &main_window::duplicates_search_performed, this, &main_window::on_duplicates_search_performed, Qt::QueuedConnection);
}

main_window::~main_window() {
    cancel_scanning();
    if (future.valid()) {
        future.wait();
    }
}

void main_window::select_directory() {
    cancel_scanning();
    if (!finished) {
        QThread::msleep(100);
    }

    if (!finished) {
        QMessageBox::information(this, "Please, wait", "It might take time to cancel current search. Wait until then.");
    } else {
        QString dir = QFileDialog::getExistingDirectory(this, "Select Directory for Scanning", QString(),
                                                        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        scan_directory(dir);
    }
}

QString pretty_presentation(std::size_t size_in_bytes) {
    static constexpr char const *sizes[]     = {"GiB", "MiB", "KiB", "B"};
    static constexpr std::size_t sizes_count = sizeof(sizes);
    static constexpr uint64_t gibibytes      = 1024ULL * 1024ULL * 1024ULL;

    uint64_t multiplier = gibibytes;

    for (size_t i = 0; i < sizes_count; i++, multiplier /= 1024) {
        if (size_in_bytes < multiplier) {
            continue;
        }
        if (size_in_bytes % multiplier == 0) {
            return QString("%1 %2").arg(size_in_bytes / multiplier).arg(sizes[i]);
        } else {
            return QString("%1 %2").arg(double(size_in_bytes) / multiplier, 0, 'f', 2).arg(sizes[i]);
        }
    }
    return "0 B";
}

void add_subtree(QTreeWidget *treeWidget, std::vector<std::string> &&duplicates_group, size_t duplicate_size) {
    QTreeWidgetItem *tree_item = new QTreeWidgetItem(treeWidget);
    assert(duplicates_group.size() > 1);
    tree_item->setText(0, duplicates_group.back().c_str());
    tree_item->setCheckState(0, Qt::Unchecked);
    tree_item->setText(1, pretty_presentation(duplicate_size));
    tree_item->setTextAlignment(1, Qt::AlignmentFlag::AlignRight);
    duplicates_group.pop_back();
    for (auto &&path : duplicates_group) {
        QTreeWidgetItem *file_item = new QTreeWidgetItem(tree_item);
        file_item->setText(0, path.c_str());
        file_item->setCheckState(0, Qt::Unchecked);
    }
    treeWidget->addTopLevelItem(tree_item);
    qApp->processEvents();
}

void main_window::scan_directory(QString const &dir) {
    if (!finished) {
        QMessageBox::information(this, "Nope", "Can't do that. Current search is not finished.");
        return;
    }

    ui->treeWidget->clear();
    ui->searchStatus->setText("Gathering files...");
    setWindowTitle(QString("Directory Content - %1").arg(dir));
    qApp->processEvents();

    cancelled = false;
    finished  = false;
    future    = std::async(std::launch::async, [this, dir]() {
        ldupes::Context context(dir.toStdString(), cancelled);
        context.set_min_file_size(1024);
        QString message;
        while (true) {
            ldupes::Error err = context.next_duplicate();
            switch (err.type) {
            case ldupes::Error::OK: {
                ui->searchStatus->setText("Comparing files...");
                add_subtree(ui->treeWidget, context.get_duplicates_group(), context.get_one_duplicate_size());
                break;
            }
            case ldupes::Error::CANCELLED: {
                message = "Cancelled";
                goto while_end;
            }
            case ldupes::Error::EOI: {
                message = "Finished";
                goto while_end;
            }
            case ldupes::Error::CANT_ACCESS: {
                message = QString("Couldn't access %1").arg(err.message);
                goto while_end;
            }
            case ldupes::Error::NOT_DIRECTORY: {
                message = dir + " is not a directory";
                goto while_end;
            }
            case ldupes::Error::OOM: {
                message = "Out of memory";
                goto while_end;
            }
            case ldupes::Error::HASHING_ERROR: {
                message = "Out of memory";
                goto while_end;
            }
            default:
                assert(false && "unexpected ld_error.type");
            }
        }
    while_end:
        finished = true;
        emit duplicates_search_performed(message);
    });
}

void main_window::on_duplicates_search_performed(QString const &message) {
    ui->searchStatus->setText(message);
}

void main_window::show_about_dialog() {
    QMessageBox::aboutQt(this);
}

void main_window::cancel_scanning() {
    cancelled = true;
}

void delete_file(QString path) {
    QFile(path).remove();
}

void main_window::ask_for_deletion() {
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirm", "Delete selected files? You might lose data");
    if (reply == QMessageBox::Yes) {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i) {
            auto group_root = ui->treeWidget->topLevelItem(i);
            for (int j = 0; j < group_root->childCount(); ++j) {
                if (group_root->child(j)->checkState(0)) {
                    delete_file(group_root->child(j)->text(0));
                    delete group_root->takeChild(j);
                    --j;
                }
            }
            if (group_root->checkState(0)) {
                delete_file(group_root->text(0));
                if (group_root->childCount() > 0) {
                    group_root->setText(0, group_root->child(0)->text(0));
                    group_root->setCheckState(0, Qt::Unchecked);
                    delete group_root->takeChild(0);
                } else {
                    delete ui->treeWidget->takeTopLevelItem(i);
                    --i;
                }
            }
        }
    }
}

void main_window::open_for_editing(QTreeWidgetItem *item, int column) {
    QDesktopServices::openUrl(QUrl::fromLocalFile(item->text(column)));
}

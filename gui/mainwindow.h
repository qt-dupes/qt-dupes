#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../ldupes-cpp/ldupes/atomics.h"

#include <QMainWindow>
#include <QTreeWidget>
#include <future>
#include <memory>

namespace Ui {
    class MainWindow;
}

class main_window : public QMainWindow {
    Q_OBJECT

  public:
    explicit main_window(QWidget *parent = nullptr);
    ~main_window();

  private slots:
    void select_directory();
    void scan_directory(QString const &dir);
    void show_about_dialog();
    void on_duplicates_search_performed(QString const &message);
    void cancel_scanning();
    void ask_for_deletion();
    void open_for_editing(QTreeWidgetItem *item, int column);

  signals:
    void duplicates_search_performed(QString const &message);

  private:
    std::unique_ptr<Ui::MainWindow> ui;
    std::future<void> future;
    atomic_bool cancelled{false};
    atomic_bool finished{true};
};

#endif // MAINWINDOW_H
